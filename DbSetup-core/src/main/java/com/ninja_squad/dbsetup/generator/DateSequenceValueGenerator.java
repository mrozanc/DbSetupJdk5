/*
 * The MIT License
 *
 * Copyright (c) 2013-2016, Ninja Squad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.ninja_squad.dbsetup.generator;

import com.ninja_squad.dbsetup.util.Preconditions;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import javax.annotation.Nonnull;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;

/**
 * A {@link ValueGenerator} that returns a sequence of dates, starting at a given zoned date time and incremented by a
 * given time, specified as an increment and a temporal unit.
 * @author JB
 */
public final class DateSequenceValueGenerator implements ValueGenerator<DateTime> {

    // the number of chars in yyyy-mm-dd hh:mm:ss
    private static final int MIN_NUMBER_OF_CHARS_FOR_TIMESTAMP = 19;

    /**
     * The available units for the increment of this sequence
     * @deprecated use ChronoField instead. This enum is only kept to maintain backward compatibility
     */
    @Deprecated
    public enum CalendarField {
        YEAR(Period.years(1)),
        MONTH(Period.months(1)),
        DAY(Period.days(1)),
        HOUR(Period.hours(1)),
        MINUTE(Period.minutes(1)),
        SECOND(Period.seconds(1)),
        MILLISECOND(Period.millis(1));

        private Period unit;

        CalendarField(Period unit) {
            this.unit = unit;
        }

        private Period toPeriod() {
            return unit;
        }
    }

    private DateTime next;
    private int increment;
    private Period unit;

    DateSequenceValueGenerator() {
        this(LocalDate.now().toDateTimeAtStartOfDay().withZone(DateTimeZone.getDefault()), 1, Period.days(1));
    }

    private DateSequenceValueGenerator(DateTime next, int increment, Period unit) {
        this.next = next;
        this.increment = increment;
        this.unit = unit;
    }

    /**
     * Restarts the sequence at the given date, in the given time zone
     * @return this instance, for chaining
     * @deprecated use one of the other <code>startingAt()</code> methods taking java.time types as argument
     */
    @Deprecated
    public DateSequenceValueGenerator startingAt(@Nonnull Date startDate, @Nonnull TimeZone timeZone) {
        Preconditions.checkNotNull(startDate, "startDate may not be null");
        Preconditions.checkNotNull(timeZone, "timeZone may not be null");
        next = new DateTime(startDate).withZone(DateTimeZone.forTimeZone(timeZone));
        return this;
    }

    /**
     * Restarts the sequence at the given date, in the default time zone
     * @return this instance, for chaining
     * @deprecated use one of the other <code>startingAt()</code> methods taking java.time types as argument
     */
    @Deprecated
    public DateSequenceValueGenerator startingAt(@Nonnull Date startDate) {
        return startingAt(startDate, TimeZone.getDefault());
    }

    /**
     * Restarts the sequence at the given date
     * @return this instance, for chaining
     * @deprecated use one of the other <code>startingAt()</code> methods taking java.time types as argument
     */
    @Deprecated
    public DateSequenceValueGenerator startingAt(@Nonnull Calendar startDate) {
        Preconditions.checkNotNull(startDate, "startDate may not be null");
        next = new DateTime(startDate).withZone(DateTimeZone.forTimeZone(startDate.getTimeZone()));
        return this;
    }

    /**
     * Restarts the sequence at the given date, in the default time zone
     * @param startDate the starting date, as a String. The supported formats are the same as the ones supported by
     * {@link com.ninja_squad.dbsetup.bind.Binders#timestampBinder()}, i.e. the formats supported by
     * <code>java.sql.Timestamp.valueOf()</code> and <code>java.sql.Date.valueOf()</code>
     * @return this instance, for chaining
     */
    public DateSequenceValueGenerator startingAt(@Nonnull String startDate) {
        Preconditions.checkNotNull(startDate, "startDate may not be null");
        if (startDate.length() >= MIN_NUMBER_OF_CHARS_FOR_TIMESTAMP) {
            return startingAt(new Date(Timestamp.valueOf(startDate).getTime()));
        }
        else {
            return startingAt(new Date(java.sql.Date.valueOf(startDate).getTime()));
        }
    }

    /**
     * Restarts the sequence at the given local date, in the default time zone
     * @return this instance, for chaining
     */
    public DateSequenceValueGenerator startingAt(@Nonnull LocalDate startDate) {
        return startingAt(startDate.toDateTimeAtStartOfDay().toLocalDateTime());
    }

    /**
     * Restarts the sequence at the given local date time, in the default time zone
     * @return this instance, for chaining
     */
    public DateSequenceValueGenerator startingAt(@Nonnull LocalDateTime startDate) {
        return startingAt(startDate.toDateTime(DateTimeZone.getDefault()));
    }

    /**
     * Restarts the sequence at the given zoned date time
     * @return this instance, for chaining
     */
    public DateSequenceValueGenerator startingAt(@Nonnull DateTime startDate) {
        next = startDate;
        return this;
    }

    /**
     * Increments the date by the given increment of the given unit.
     * @return this instance, for chaining
     * @deprecated use the other {@link #incrementingBy(int, Period)} method
     */
    @Deprecated
    public DateSequenceValueGenerator incrementingBy(int increment, @Nonnull CalendarField unit) {
        Preconditions.checkNotNull(unit, "unit may not be null");
        return incrementingBy(increment, unit.toPeriod());
    }

    /**
     * Increments the date by the given increment of the given unit. One of the constants of ChronoField is typically
     * used for the unit.
     * @return this instance, for chaining
     */
    public DateSequenceValueGenerator incrementingBy(int increment, @Nonnull Period unit) {
        Preconditions.checkNotNull(unit, "unit may not be null");
        this.increment = increment;
        this.unit = unit;
        return this;
    }

    public DateTime nextValue() {
        DateTime result = next;
        next = next.plus(unit.multipliedBy(increment));
        return result;
    }

    @Override
    public String toString() {
        return "DateSequenceValueGenerator["
               + "next=" + next
               + ", increment=" + increment
               + ", unit=" + unit
               + "]";
    }
}
